package enginex.controllers;

import java.util.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import enginex.entities.EntityManager;
import enginex.entities.product.Product;
import enginex.entities.product.ProductDAO;


public class InitializeServlet extends HttpServlet {
	
	static ProductDAO pdao = new ProductDAO();
	static EntityManager man = new EntityManager();
	static Product initK = new Product();
	static Product initL = new Product();
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.getWriter().println("Starting");
		initX();
		resp.getWriter().println(initK.toString());
		resp.getWriter().println(initL.toString());
		resp.getWriter().println("Finishing");
		

		

	}
	
	@Override	
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException{		
	}
	
	public static void initX(){
	  initK = pdao.getProduct("Wendigo");
	  initL = pdao.getProductByKey("BMW-N55-Engine-011");
	  pdao.updateStock("Wendigo", -12);
	}
	  
		
	
}
