package enginex.controllers.stock;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import enginex.entities.EntityManager;
import enginex.entities.product.Product;
import enginex.entities.product.ProductDAO;

public class StockManagementController extends HttpServlet {
	
	static ProductDAO pdao = new ProductDAO();
	
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        
        
		RequestDispatcher requestDispatcher = req.getRequestDispatcher("/stock_managementX");
		try {
			requestDispatcher.forward(req, resp);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		ProductDAO pdao = new ProductDAO();
		String amount = req.getParameter("stockLevel");
		int intAmount = Integer.parseInt(amount);
        String codeName = req.getParameter("engineType");
        String action = req.getParameter("action");
        
        System.out.println(amount+"\n"+codeName+"\n"+action);
        
        if(action == "AddOrdered"){
        	
        }
        else if(action == "AddProduced"){
        	
        }
        else if(action == "RemoveError"){
        	intAmount = (0-intAmount);
        }
        
        pdao.updateStock(codeName, intAmount);
		
		RequestDispatcher requestDispatcher = req.getRequestDispatcher("/stock_managementX");
		try {
			requestDispatcher.forward(req, resp);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
