package enginex.entities;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.*;

import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;

import java.util.ArrayList;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.KeyRange;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;

import enginex.entities.product.Product;


public class EntityManager {
	String resp,req;
	DatastoreService xt = DatastoreServiceFactory.getDatastoreService();
	Entity buffer = null;

	public void createProducts(List<Product> listOfProducts){
		for (Product pmEntity : listOfProducts) {
			buffer = new Entity("Product",pmEntity.getEngineCode());
			buffer.setProperty("Description", pmEntity.getDescription());
			buffer.setProperty("EngineCode", pmEntity.getEngineCode());
			buffer.setProperty("CodeName", pmEntity.getCodeName());
			
			buffer.setProperty("StockLevel", pmEntity.getStockLevel());
			buffer.setProperty("PendingProduction", pmEntity.getPendingProduction());
			buffer.setProperty("PendingOrders", pmEntity.getPendingOrders());
			xt.put(buffer);
		}
	}
	
	
	
	public void updateProduct(Product pmEntity){
			buffer = new Entity("Product",pmEntity.getEngineCode());
			buffer.setProperty("Description", pmEntity.getDescription());
			buffer.setProperty("CodeName", pmEntity.getCodeName());
			buffer.setProperty("StockLevel", pmEntity.getStockLevel());
			buffer.setProperty("PendingProduction", pmEntity.getPendingProduction());
			buffer.setProperty("PendingOrders", pmEntity.getPendingOrders());
			xt.put(buffer);
	}
	
	

	
	public PreparedQuery getEntity(String Kind, String PropertyName, String PropertyValue){
		
		@SuppressWarnings("deprecation")
		Query q = new Query(Kind).addFilter(PropertyName,Query.FilterOperator.EQUAL,PropertyValue);
		PreparedQuery pq = xt.prepare(q);
		
		return pq;
	}
	
	public Object getEntityByKey(String Kind, String key){
	    Key entityKey = KeyFactory.createKey(Kind, key);
	    Entity object = null;
		try {
			object = xt.get(entityKey);
		} catch (EntityNotFoundException e) {
			System.out.println(e.getMessage());
		}

		return object;
	}
	
	public PreparedQuery getAllEntities(String Kind){
		Query q = new Query(Kind);
		PreparedQuery pq = xt.prepare(q);
		
		return pq;
	}
	
	public void tester(){
	

	
	//Single Unique Entitiy

	Entity e = new Entity("User",122);
	e.setProperty("UserId", 122);
	e.setProperty("FirstName", "Dehan");
	e.setProperty("LastName", "De Croos XTT");
	
	
	xt.put(e);



	
/*		//Batch persist
	Entity e1 = new Entity("Entity1");
	Entity e2 = new Entity("Entity2");
	Entity e3 = new Entity("Entity3");
	List<Entity> eList = Arrays.asList(e1,e2,e3);
	xt.put(eList);
	
	//ReferringID
	KeyRange range = xt.allocateIds("Entity", 1);
	Key e1Key = range.getStart();
	Entity e1w = new Entity(e1Key);
	Entity e2w = new Entity("Entity");
	e2.setProperty("reference", e1Key);
	xt.put(new ArrayList<Entity>(Arrays.asList(e1w, e2w)));
	
	//Sequencing IDXXX
	KeyRange ran1 = xt.allocateIds("seqKeydEntity", 100);
	Key keyRecieved = ran1.getStart();
	Entity entX = new Entity("seqKeydEntity",keyRecieved);
	entX.setProperty("ID", keyRecieved);
	xt.put(entX);*/
	
	//ancestor path
/*		Entity person1 = new Entity("GreatGrandpa");
	xt.put(person1);
	Entity person2 = new Entity("Grandpa", person1.getKey());
	xt.put(person2)
	;
	Entity person3 = new Entity("father", person2.getKey());
	xt.put(person3);
	Entity person4 = new Entity("me", person3.getKey());
	xt.put(person4);		
	
	resp.getWriter().println(person4.getKind()+" "+person4.getParent().getKind());*/
	//datastore.delete(employeeKey);
	}
}
