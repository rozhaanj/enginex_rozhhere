package enginex.entities;

public class RolesFactory {

	public Rolls makeRole(String myRole, String username, String password){
		Rolls role=null;
		
		switch (myRole) {
		case "admin":
			role = new Admin();
			role.setUsername(username);
			role.setPassword(password);
			
			break;
		case "cost":
			role = new CostManager();
			role.setUsername(username);
			role.setPassword(password);
			break;
		case "sales":
			role = new SalesManager();
			role.setUsername(username);
			role.setPassword(password);
			break;
		case "stores":
			role = new StoresManager();
			role.setUsername(username);
			role.setPassword(password);
			break;

		default:
			break;
		}
		
		return role;
		
	}
}
