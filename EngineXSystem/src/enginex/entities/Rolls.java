package enginex.entities;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public abstract class Rolls {
	@Id
	private String username;
	private String password;
	private String link;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	
}
