package enginex.entities;

public class StoresManager extends Rolls {

	private String link="authentication/storesmgr.html";
	
	@Override
	public String toString() {
		
		return "Stores Manager";
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}
}
