package enginex.entities.product;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Product {
	public Product(){};
	public Product(String engineCode, String description, String codeName,
			int stockLevel, int pendingProduction, int pendingOrders) {
		super();
		this.engineCode = engineCode;
		this.description = description;
		this.codeName = codeName;
		StockLevel = stockLevel;
		PendingProduction = pendingProduction;
		PendingOrders = pendingOrders;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String engineCode;
	private String description;
	private String codeName;
	private int StockLevel;
	private int PendingProduction;
	private int PendingOrders;
	
	@Override
	public String toString() {
		return engineCode+" \n "+description+" \n "+codeName+" \n "+StockLevel+" \n "+PendingProduction+" \n "+PendingOrders;
		
	}
	
	public String getEngineCode() {
		return engineCode;
	}
	public void setEngineCode(String engineCode) {
		this.engineCode = engineCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCodeName() {
		return codeName;
	}
	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}
	public int getStockLevel() {
		return StockLevel;
	}
	public void setStockLevel(int stockLevel) {
		StockLevel = stockLevel;
	}
	public int getPendingProduction() {
		return PendingProduction;
	}
	public void setPendingProduction(int pendingProduction) {
		PendingProduction = pendingProduction;
	}
	public int getPendingOrders() {
		return PendingOrders;
	}
	public void setPendingOrders(int pendingOrders) {
		PendingOrders = pendingOrders;
	}
	


}
