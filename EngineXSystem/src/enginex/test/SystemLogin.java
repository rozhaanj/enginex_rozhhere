package enginex.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

@SuppressWarnings("serial")
public class SystemLogin extends HttpServlet {
	
	 //User user=null;
	DatastoreService datastore=DatastoreServiceFactory.getDatastoreService();
	
	
	
	@Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
              throws IOException {
     UserService userService = UserServiceFactory.getUserService();
     User user = userService.getCurrentUser();
     
        if (user != null) {
        	
        	String temp = isSystemUser(user);
        	
            resp.setContentType("text/plain");
            resp.getWriter().println("Hello, " + isSystemUser(user));
        } else {
            resp.sendRedirect(userService.createLoginURL(req.getRequestURI()));
        }
	}
	
	public String isSystemUser(User user){
		
		//String userEmailId= user.getNickname();
		
		/*Filter filter =
				  new Filter("UserId",
				                      FilterOperator.EQUAL,
				                      userEmailId);*/
		
		Filter filter = new FilterPredicate("UserName",FilterOperator.EQUAL,user.getNickname());

		Query q = new Query("SystemUser").setFilter(filter);
						
		PreparedQuery pq = datastore.prepare(q);

		Entity result = pq.asSingleEntity();//if there are more than one result you MUST use a List
		try{
		String role= result.getProperty("Roles").toString();
		return role;
		}catch(NullPointerException e){
			return "You Haven't permission OR check from another account";
		}
		
		
		
	}
	
	/*public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(req.getInputStream()));
		
		resp.setContentType("text/plain");
		resp.getWriter().println("Hello, Admin ABC get"+" "+br.readLine());
	}
	
	@Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {
       
		BufferedReader br = new BufferedReader(new InputStreamReader(req.getInputStream()));
		String reqString = br.readLine();
		String[] reqStringsArray = reqString.split("&");
		String[] userName=reqStringsArray[0].split("=");
		
		resp.setContentType("text/plain");
		resp.getWriter().println("Hello, Admin ABC Post"+" "+userName[1]);
        
    }*/
	
}
