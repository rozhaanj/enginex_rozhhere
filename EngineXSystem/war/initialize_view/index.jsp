<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- The HTML 4.01 Transitional DOCTYPE declaration-->
<!-- above set at the top of the file will set     -->
<!-- the browser's rendering engine into           -->
<!-- "Quirks Mode". Replacing this declaration     -->
<!-- with a "Standards Mode" doctype is supported, -->
<!-- but may lead to some differences in layout.   -->

<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>Vehicle Management System</title>
  </head>

  <body>
    <h1>Init App Engine!</h1>
	
    <table>      
      <tr>        
        <td><a href="intialize.do">Start Init</a></td>
      </tr>
	  <form action="intialize.do" method="get">
		  First name: <input type="text" name="fname"><br>
		  Last name: <input type="text" name="lname"><br>
	  <input type="submit" value="Submit">
</form>

  </body>
</html>
