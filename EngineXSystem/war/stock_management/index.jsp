<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- The HTML 4.01 Transitional DOCTYPE declaration-->
<!-- above set at the top of the file will set     -->
<!-- the browser's rendering engine into           -->
<!-- "Quirks Mode". Replacing this declaration     -->
<!-- with a "Standards Mode" doctype is supported, -->
<!-- but may lead to some differences in layout.   -->

<html>
  <head>
    <meta http-equiv="content-type" content="text/html"; charset="UTF-8">
    <link href="../bootstraplib/css/bootstrap.css" type="text/css" rel="stylesheet">
    <script src="../Jquery/jquery-2.1.0.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {                        
      $('#submit').click(
      function(event) {  
        var amount=$('#stockLevel').val();
        var addrem = $('#addRemove').val();
        var engineT = $('input[name=engineType]:checked', '#stockAlterForm').val();

        var dataX = amount + ' ' + addrem+ ' ' +engineT;
        dataX
        //alert(data);

        $("#QBeast").html("go");
        $.ajax({
          type: "POST",
          url: "test_ajax",
          data: dataX,
          success:function(result){
          alert("result")
          }
        });
      });
    });
      
    </script>
    </head>
    <title>Hello App Engine</title>
  </head>

  <body>
<div class="page-header">
  <h1 align="center">Vehicle Stock Management</h1>
</div>

<div class="col-md-3"></div>
<div class="col-md-6">
	<table align="center" class="table table-bordered table-striped">
		<thead>
          <tr>
            <th>Engine</th>
            <th>Code Name</th>
            <th>In Stock</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>EN-4478 </td>
            <td><code>Beast</code></td>
            <td id ="QBeasttd"><p id="QBeast">80</p></td>
          </tr>
          <tr>
            <td>EN-1023</td>
            <td><code>Wendigo</code></td>
            <td id ="QWendigo">30</td>
          </tr>
          <tr>
            <td>EN-2223</td>
            <td><code>Nevada</code></td>
            <td id ="QNevada">60</td>
          </tr>
          <tr>
            <td>EN-1100</td>
            <td><code>Dragon</code></td>
            <td id ="QDragon">50</td>
          </tr>
        </tbody>
      </table>
</div>
<div class="col-md-3"></div>

<div class="col-md-4"></div>
<div class="col-md-4">  
  <form id="stockAlterForm" class="form-horizontal" role="form" method="POST">
        <legend>Alter Stock</legend>
    <div class="form-group">
      <label for="stockLevelLabel" class="col-sm-2 control-label">Amount</label>
      <div class="col-sm-10">
        <input type="text" name="stockLevel" class="form-control" id="stockLevel" placeholder="Enter Value">
      </div>

      <div class="col-md-2"></div>
      <div class="col-md-4">
        <div class="radio">
          <label>
            <input type="radio" name="engineType" id="optBeast" value="Beast">
              Beast
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="engineType" id="optWendigo" value="Wendigo">
              Wendigo
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="engineType" id="optNevada" value="Nevada">
              Nevada
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="engineType" id="optDragon" value="Dragon">
              Dragon
          </label>
       </div>
      </div>
      <div class="col-md-5">
        <label for="radioLabel" class="col-sm-2 control-label">Add/Remove</label>
          <select class="form-control" name="action" id="addRemove">
            <option value="Add">Add</option>
            <option value="Remove">Remove</option>
          </select>
        <button class="btn btn-default" style="margin-bottom:2%;" id="submit">Submit</button>
      </div>
      <div class="col-md-1"></div>
    </div>    
  </form>
</div>
<div id="Test">This should change</div>



</body>
</html>
