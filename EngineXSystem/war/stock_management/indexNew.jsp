<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="enginex.entities.product.ProductDAO"%>
<%@page import="enginex.entities.product.*"%>
<%@page import="java.util.Map"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.*" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ page import="com.google.appengine.api.datastore.DatastoreServiceFactory" %>
<%@ page import="com.google.appengine.api.datastore.DatastoreService" %>
<%@ page import="com.google.appengine.api.datastore.Query" %>
<%@ page import="com.google.appengine.api.datastore.Entity" %>
<%@ page import="com.google.appengine.api.datastore.FetchOptions" %>
<%@ page import="com.google.appengine.api.datastore.Key" %>
<%@ page import="com.google.appengine.api.datastore.KeyFactory" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%

	ProductDAO jspProdDao = new ProductDAO();
	List<Product> liPro = jspProdDao.getAllProducts();
	Map<String, Integer> stockMap = new HashMap<String, Integer>();
	for (Product var : liPro){
		stockMap.put(var.getCodeName(), var.getStockLevel());
	}


%>

<html>
  <head>
    <meta http-equiv="content-type" content="text/html"; charset="UTF-8">
    <link href="../bootstraplib/css/bootstrap.css" type="text/css" rel="stylesheet">
    </head>
    <title>Hello App Engine</title>
  </head>

  <body>
  
  
<div class="page-header">
  <h1 align="center">Vehicle Stock Management</h1>
  ${fn:escapeXml(user.nickname)}
</div>

<div class="col-md-3"></div>
<div class="col-md-6">
	<table id="stockTable" align="center" class="table table-bordered table-striped">
		<thead>
          <tr>
            <th>Engine</th>
            <th>Code Name</th>
            <th>In Stock</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>EN-4478 </td>
            <td><code>Beast</code></td>
            <td id ="QBeast"><% stockMap.get("Beast"); %></td>
          </tr>
          <tr>
            <td>EN-1023</td>
            <td><code>Wendigo</code></td>
            <td id ="QWendigo"><% stockMap.get("Wendigo"); %></td>
          </tr>
          <tr>
            <td>EN-2223</td>
            <td><code>Nevada</code></td>
            <td id ="QNevada"><% stockMap.get("Nevada"); %></td>
          </tr>
          <tr>
            <td>EN-1100</td>
            <td><code>Dragon</code></td>
            <td id ="QDragon"><% stockMap.get("Dragon"); %></td>
          </tr>
        </tbody>
      </table>
</div>
<div class="col-md-3"></div>

<div class="col-md-4"></div>
<div class="col-md-4">  
  
  <form id="stockAlterForm" class="form-horizontal" role="form" action="stock.do"  method="POST" >
        <legend>Alter Stock</legend>
    <div class="form-group">
      <label for="stockLevelLabel" class="col-sm-2 control-label">Amount</label>
      <div class="col-sm-10">
        <input type="text" name="stockLevel" class="form-control" id="stockLevel" placeholder="Enter Value">
      </div>

      <div class="col-md-2"></div>
      <div class="col-md-4">
        <div form="stockAlterForm" class="radio">
          <label>
            <input form="stockAlterForm" type="radio" name="engineType" id="optBeast" value="Beast">
              Beast
          </label>
        </div>
        <div class="radio">
          <label>
            <input form="stockAlterForm" type="radio" name="engineType" id="optWendigo" value="Wendigo">
              Wendigo
          </label>
        </div>
        <div class="radio">
          <label>
            <input form="stockAlterForm" type="radio" name="engineType" id="optNevada" value="Nevada">
              Nevada
          </label>
        </div>
        <div class="radio">
          <label>
            <input form="stockAlterForm" type="radio" name="engineType" id="optDragon" value="Dragon">
              Dragon
          </label>
       </div>
      </div>
      <div class="col-md-5">
        <label for="radioLabel" class="col-sm-2 control-label">Add/Remove</label>
          <select form="stockAlterForm" class="form-control" name="action" id="addRemove">
            <option value="AddProduced">Add From Production</option>
            <option value="AddOrdered">Add From Orders</option>
            <option value="RemoveError">Remove Error</option>
          </select>
        <button class="btn btn-default" form="stockAlterForm" type="submit" style="margin-bottom:2%;" id="submit">Submit</button>
      </div>
      <div class="col-md-1"></div>
    </div>    
  </form>
</div>


</body>
</html>
